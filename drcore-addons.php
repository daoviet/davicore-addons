<?php
/**
 * Plugin Name: DrCore Addons
 * Plugin URI: http://drfuri.com/plugins/drcore-addons.zip
 * Description: Extra elements for Visual Composer. It was built for DrCore theme.
 * Version: 1.0.0
 * Author: Drfuri
 * Author URI: http://drfuri.com/
 * License: GPL2+
 * Text Domain: drcore
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'DRCORE_ADDONS_DIR' ) ) {
	define( 'DRCORE_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'DRCORE_ADDONS_URL' ) ) {
	define( 'DRCORE_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}

require_once DRCORE_ADDONS_DIR . '/inc/visual-composer.php';
require_once DRCORE_ADDONS_DIR . '/inc/shortcodes.php';
require_once DRCORE_ADDONS_DIR . '/inc/portfolio.php';
require_once DRCORE_ADDONS_DIR . '/inc/services.php';
require_once DRCORE_ADDONS_DIR . '/inc/socials.php';

if ( is_admin() ) {
	require_once DRCORE_ADDONS_DIR . '/inc/importer.php';
}

/**
 * Init
 */
function drcore_vc_addons_init() {
	load_plugin_textdomain( 'THEME_DOMAIN', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new DrCore_VC;
	new DrCore_Shortcodes;
	new DrCore_Portfolio;
	new DrCore_Services;
}

add_action( 'after_setup_theme', 'drcore_vc_addons_init', 20 );

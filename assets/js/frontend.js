(function ( $ ) {
	'use strict';

	var davicore = davicore || {};

	davicore.init = function () {
		davicore.$body = $( document.body ),
			davicore.$window = $( window ),
			davicore.$header = $( '#masthead' );
		
	};

	
	/**
	 * Document ready
	 */
	$( function () {
		davicore.init();
	} );

})( jQuery );
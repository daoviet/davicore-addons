<?php

/**
 * Define theme shortcodes
 *
 * @package DaviCore
 */
class DaviCore_Shortcodes {

	/**
	 * Store variables for js
	 *
	 * @var array
	 */
	public $l10n = array();

	/**
	 * Store variables for maps
	 *
	 * @var array
	 */
	public $maps = array();
	public $api_key = '';

	/**
	 * Check if WooCommerce plugin is actived or not
	 *
	 * @var bool
	 */
	private $wc_actived = false;

	/**
	 * Construction
	 *
	 * @return Davicore_Shortcodes
	 */
	function __construct() {
		$this->wc_actived = function_exists( 'is_woocommerce' );

		$shortcodes = array(
			'davicore_empty_space',
		);

		foreach ( $shortcodes as $shortcode ) {
			add_shortcode( $shortcode, array( $this, $shortcode ) );
		}

		add_action( 'wp_footer', array( $this, 'footer' ) );
	}

	public function footer() {
		// Load Google maps only when needed
		if ( isset( $this->l10n['map'] ) ) {
			echo '<script>if ( typeof google !== "object" || typeof google.maps !== "object" )
				document.write(\'<script src="//maps.google.com/maps/api/js?sensor=false&key=' . $this->api_key . '"><\/script>\')</script>';
		}

		wp_enqueue_script(
			'shortcodes', DAVICORE_ADDONS_URL . 'assets/js/frontend.js', array(
			'jquery',
		), '20171018', true
		);

		wp_localize_script( 'shortcodes', 'davicoreShortCode', $this->l10n );
	}

	/**
	 * Get empty space
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function davicore_empty_space( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'height'        => '',
				'height_mobile' => '',
				'height_tablet' => '',
				'height_medium' => '',
				'bg_color'      => '',
				'el_class'      => '',
			), $atts
		);

		$css_class = array(
			'davicore-empty-space',
			$atts['el_class'],
		);

		$style = '';

		if ( $atts['bg_color'] ) {
			$style = 'background-color:' . $atts['bg_color'] . ';';
		}

		$height = $atts['height'] ? (float) $atts['height'] : 0;

		if ( ! empty( $atts['height_medium'] ) || $atts['height_medium'] == '0' ) {
			$height_medium = (float) $atts['height_medium'];
		} else {
			$height_medium = $height;
		}

		if ( ! empty( $atts['height_tablet'] ) || $atts['height_tablet'] == '0' ) {
			$height_tablet = (float) $atts['height_tablet'];
		} else {
			$height_tablet = $height_medium;
		}

		if ( ! empty( $atts['height_mobile'] ) || $atts['height_mobile'] == '0' ) {
			$height_mobile = (float) $atts['height_mobile'];
		} else {
			$height_mobile = $height_tablet;
		}

		$inline_css        = $height >= 0.0 ? ' style="height: ' . esc_attr( $height ) . 'px"' : '';
		$inline_css_medium = $height_medium >= 0.0 ? ' style="height: ' . esc_attr( $height_medium ) . 'px"' : '';
		$inline_css_tablet = $height_tablet >= 0.0 ? ' style="height: ' . esc_attr( $height_tablet ) . 'px"' : '';
		$inline_css_mobile = $height_mobile >= 0.0 ? ' style="height: ' . esc_attr( $height_mobile ) . 'px"' : '';

		return sprintf(
			'<div class="%s" style="%s">' .
			'<div class="davicore_empty_space_lg hidden-md hidden-sm hidden-xs" %s></div>' .
			'<div class="davicore_empty_space_md hidden-lg hidden-sm hidden-xs" %s></div>' .
			'<div class="davicore_empty_space_sm hidden-lg hidden-md hidden-xs" %s></div>' .
			'<div class="davicore_empty_space_xs hidden-lg hidden-md hidden-sm" %s></div>' .
			'</div>',
			esc_attr( implode( ' ', $css_class ) ),
			$style,
			$inline_css,
			$inline_css_medium,
			$inline_css_tablet,
			$inline_css_mobile
		);
	}

	/**
	 * Get limited words from given string.
	 * Strips all tags and shortcodes from string.
	 *
	 * @since 1.0.0
	 *
	 * @param integer $num_words The maximum number of words
	 * @param string $more More link.
	 *
	 * @return string Limited content.
	 */
	protected function davicore_addons_content_limit( $content, $num_words, $more = "&hellip;" ) {
		// Strip tags and shortcodes so the content truncation count is done correctly
		$content = strip_tags( strip_shortcodes( $content ), apply_filters( 'davicore_content_limit_allowed_tags', '<script>,<style>' ) );

		// Remove inline styles / scripts
		$content = trim( preg_replace( '#<(s(cript|tyle)).*?</\1>#si', '', $content ) );

		// Truncate $content to $max_char
		$content = wp_trim_words( $content, $num_words );

		if ( $more ) {
			return sprintf(
				'<div class="excerpt">%s <a href="%s" class="more-link" title="%s">%s</a></div>',
				$content,
				get_permalink(),
				sprintf( esc_html__( 'Continue reading &quot;%s&quot;', THEME_DOMAIN ), the_title_attribute( 'echo=0' ) ),
				esc_html( $more )
			);
		}

		return sprintf( '<div class="excerpt">%s</div>', $content );

	}

	/**
	 * Get vc link
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	protected function get_vc_link( $atts, $content ) {
		$attributes = array(
			'class' => 'davicore-link',
		);

		$link = vc_build_link( $atts['link'] );

		if ( ! empty( $link['url'] ) ) {
			$attributes['href'] = $link['url'];
		}

		if ( ! $content ) {
			$content             = $link['title'];
			$attributes['title'] = $content;
		}

		if ( ! empty( $link['target'] ) ) {
			$attributes['target'] = $link['target'];
		}

		if ( ! empty( $link['rel'] ) ) {
			$attributes['rel'] = $link['rel'];
		}

		$attr = array();

		foreach ( $attributes as $name => $v ) {
			$attr[] = $name . '="' . esc_attr( $v ) . '"';
		}

		$button = sprintf(
			'<%1$s %2$s>%3$s</%1$s>',
			empty( $attributes['href'] ) ? 'span' : 'a',
			implode( ' ', $attr ),
			$content
		);

		return $button;
	}

	/**
	 * @param        $image
	 * @param string $size
	 *
	 * @return string
	 */

	protected function get_vc_image( $image, $size = 'thumbnail' ) {
		$image_src = '';
		if ( function_exists( 'wpb_getImageBySize' ) ) {
			$image = wpb_getImageBySize(
				array(
					'attach_id'  => $image,
					'thumb_size' => $size,
				)
			);

			if ( $image['thumbnail'] ) {
				$image_src = $image['thumbnail'];
			} elseif ( $image['p_img_large'] ) {
				$image_src = sprintf( '<img src="%s">', esc_url( $image['p_img_large'][0] ) );
			}

		}

		if ( empty( $image_src ) ) {
			$image_src = wp_get_attachment_image( $image, $size );
		}

		return $image_src;
	}
}

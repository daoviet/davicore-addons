<?php
/**
 * Custom functions for Visual Composer
 *
 * @package    DaviCore
 * @subpackage Visual Composer
 */

if ( ! function_exists( 'is_plugin_active' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

/**
 * Class Davicore
 *
 * @since 1.0.0
 */
class DaviCore_VC {

	/**
	 * Temporary cached terms variable
	 *
	 * @var array
	 */
	public $terms = array();

	/**
	 * Construction
	 */
	function __construct() {
		// Stop if VC is not installed
		if ( ! is_plugin_active( 'js_composer/js_composer.php' ) ) {
			return false;
		}


		add_action( 'init', array( $this, 'map_shortcodes' ), 20 );
	}

	/**
	 * Add new params or add new shortcode to VC
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	function map_shortcodes() {

		// Empty Space
		vc_map(
			array(
				'name'     => esc_html__( 'Davicore Empty Space', THEME_DOMAIN ),
				'base'     => 'davicore_empty_space',
				'class'    => '',
				'category' => esc_html__( THEME_DOMAIN, THEME_DOMAIN ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height(px)', THEME_DOMAIN ),
						'param_name'  => 'height',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Desktop. ( 1200px and up )', THEME_DOMAIN )
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height on Medium devices (px)', THEME_DOMAIN ),
						'param_name'  => 'height_medium',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Medium devices. ( 992px and up )', THEME_DOMAIN )
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height on Tablet(px)', THEME_DOMAIN ),
						'param_name'  => 'height_tablet',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Mobile. Leave empty to use the height of the desktop. ( 768px and up )', THEME_DOMAIN )
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height on Mobile(px)', THEME_DOMAIN ),
						'param_name'  => 'height_mobile',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Mobile. Leave empty to use the height of the tablet. ( less than 768px )', THEME_DOMAIN )
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Background Color', THEME_DOMAIN ),
						'param_name' => 'bg_color',
						'value'      => '',
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', THEME_DOMAIN ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', THEME_DOMAIN ),
					),
				),
			)
		);
	}
}
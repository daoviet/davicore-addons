<?php
/**
 * Hooks for share socials
 *
 * @package Davicore
 */

if ( ! function_exists( 'davicore_addons_share_link_socials' ) ) :
	function davicore_addons_share_link_socials( $socials, $title, $link, $media ) {
		$socials_html = '';
		if ( $socials ) {
			if ( in_array( 'facebook', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-facebook davicore-facebook" title="%s" href="http://www.facebook.com/sharer.php?u=%s&t=%s" target="_blank"><i class="ion-social-facebook"></i></a></li>',
					esc_attr( $title ),
					urlencode( $link ),
					urlencode( $title )
				);
			}

			if ( in_array( 'twitter', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-twitter davicore-twitter" href="http://twitter.com/share?text=%s&url=%s" title="%s" target="_blank"><i class="ion-social-twitter"></i></a></li>',
					urlencode( $title ),
					urlencode( $link ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'pinterest', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-pinterest davicore-pinterest" href="http://pinterest.com/pin/create/button?media=%s&url=%s&description=%s" title="%s" target="_blank"><i class="ion-social-pinterest"></i></a></li>',
					urlencode( $media ),
					urlencode( $link ),
					urlencode( $title ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'google', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-google-plus davicore-google-plus" href="https://plus.google.com/share?url=%s&text=%s" title="%s" target="_blank"><i class="ion-social-googleplus"></i></a></li>',
					urlencode( $link ),
					urlencode( $title ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'linkedin', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-linkedin davicore-linkedin" href="http://www.linkedin.com/shareArticle?url=%s&title=%s" title="%s" target="_blank"><i class="ion-social-linkedin"></i></a></li>',
					urlencode( $link ),
					urlencode( $title ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'tumblr', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-tumblr davicore-tumblr" href="http://www.tumblr.com/share/link?url=%s" title="%s" target="_blank"><i class="ion-social-tumblr"></i></a></li>',
					urlencode( $link ),
					esc_attr( $title )
				);
			}
		}

		if ( $socials_html ) {
			return sprintf( '<ul class="davicore-social-share socials-inline">%s</ul>', $socials_html );
		}
		?>
		<?php
	}

endif;


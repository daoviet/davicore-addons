<?php
/**
 * Plugin Name: DaviCore Addons
 * Plugin URI: http://drfuri.com/plugins/davicore-addons.zip
 * Description: Extra elements for Visual Composer. It was built for DaviCore theme.
 * Version: 1.0.0
 * Author: Drfuri
 * Author URI: http://drfuri.com/
 * License: GPL2+
 * Text Domain: davicore
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'DAVICORE_ADDONS_DIR' ) ) {
	define( 'DAVICORE_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'DAVICORE_ADDONS_URL' ) ) {
	define( 'DAVICORE_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}

require_once DAVICORE_ADDONS_DIR . '/inc/visual-composer.php';
require_once DAVICORE_ADDONS_DIR . '/inc/shortcodes.php';
require_once DAVICORE_ADDONS_DIR . '/inc/portfolio.php';
require_once DAVICORE_ADDONS_DIR . '/inc/services.php';
require_once DAVICORE_ADDONS_DIR . '/inc/socials.php';

if ( is_admin() ) {
	require_once DAVICORE_ADDONS_DIR . '/inc/importer.php';
}

/**
 * Init
 */
function davicore_vc_addons_init() {
	load_plugin_textdomain( 'THEME_DOMAIN', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new DaviCore_VC;
	new DaviCore_Shortcodes;
	new DaviCore_Portfolio;
	new DaviCore_Services;
}

add_action( 'after_setup_theme', 'davicore_vc_addons_init', 20 );
